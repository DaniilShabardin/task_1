import QtQuick 2.15
import QtQuick.Window 2.15

Window{
    id: root
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: "Make transition"

    Rectangle{
        id: scene
        anchors.fill: parent
        state: "OtherState"

        Rectangle{
            id: leftRectangle
            x: 100
            y: 200
            color: "lightgrey"
            width: 100
            height: 100
            border.color: "black"
            border.width: 3
            radius: 5

            Text {
                id: name_move
                anchors.centerIn: parent
                text: "move"
            }

            MouseArea{
                anchors.fill: parent
                onClicked:{
                    if(scene.state == "LeftState"){

                        scene.state = "OtherState";

                        if(ball.x == leftRectangle.x + 5) ball.x += 10;
                    }

                    else ball.x += 10;

                    if(ball.x > rightRectangle.x)scene.state = "LeftState";
                }
            }
        }

        Rectangle{
            id: rightRectangle
            x: 300
            y: 200
            color: "lightgrey"
            width: 100
            height: 100
            border.color: "black"
            border.width: 3
            radius: 5

            Text {
                id: name_return
                anchors.centerIn: parent
                text: "return"
            }

            MouseArea{
                anchors.fill: parent
                onClicked: scene.state = "LeftState"
            }
        }

        Rectangle{
            id: ball
            x: leftRectangle.x + 5
            y: leftRectangle.y + 5
            color: "darkgreen"
            width: leftRectangle.width - 10
            height: leftRectangle.height - 10
            radius: width / 2
        }

        states: [

        State {
                name: "LeftState"
                PropertyChanges {
                    target: ball
                    x:leftRectangle.x + 5
                }
            },

         State {
                 name: "OtherState"
                 PropertyChanges {
                 target: ball
                 x: ball.x
                 }
            }
        ]

        transitions: [

            Transition {
                from: "OtherState"
                to: "LeftState"

                NumberAnimation {
                    property: "x"
                    duration: 1000
                    easing.type: Easing.InOutExpo
                }
            }
        ]
    }
}
